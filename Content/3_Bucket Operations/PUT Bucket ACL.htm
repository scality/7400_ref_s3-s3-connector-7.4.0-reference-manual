<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="6" MadCap:lastHeight="851.3333" MadCap:lastWidth="648">
  <head>
    <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
  </head>
  <body>
    <h1 MadCap:autonum="1.  ">PUT Bucket ACL</h1>
    <p style="text-align: left;">The PUT Bucket ACL operation uses the <span class="ElementName">acl</span> subresource to set the permissions on an existing bucket using its access control list (ACL).</p>
    <p>WRITE_ACP access is required to set the ACL of a bucket.</p>
    <p style="text-align: left;page-break-after: avoid;">Bucket permissions are set using one of the following two methods:</p>
    <ul>
      <li>Specifying the ACL in the request body</li>
      <li>Specifying permissions using request headers</li>
    </ul>
    <p class="BoxIMPORTANT">Access permission cannot be specified using both the request body and the request headers.</p>
    <h2 MadCap:autonum="1.1.  ">Requests</h2>
    <h3 MadCap:autonum="1.1.1  ">Request Syntax</h3>
    <p style="text-align: left;">The request syntax that follows is for sending the ACL in the request body. If headers will be used to specify the permissions for the bucket,  the ACL cannot be sent in the request body (refer to <MadCap:xref href="../8_API Knowledge Primers/Request Headers.htm">“Request Headers” on page 1</MadCap:xref> for a list of available headers).</p>
    <pre xml:space="preserve">PUT /?acl HTTP/1.1
Host: {{BucketName}}.{{StorageService}}.com
Date: {{date}}
Authorization: {{authorizationString}}

&lt;AccessControlPolicy&gt;
  &lt;Owner&gt;
    &lt;ID&gt;ID&lt;/ID&gt;
    &lt;DisplayName&gt;EmailAddress&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;ID&lt;/ID&gt;
        &lt;DisplayName&gt;EmailAddress&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;Permission&lt;/Permission&gt;
    &lt;/Grant&gt;
    ...
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt; </pre>
    <h3 MadCap:autonum="1.1.2  ">Request Parameters</h3>
    <p style="text-align: left;">The PUT Bucket ACL operation does not use request parameters.</p>
    <h3 MadCap:autonum="1.1.3  ">Request Headers</h3>
    <p style="text-align: left;"> The PUT Bucket ACL operation can use a number of optional request headers in addition to those that are common to all operations (refer to <MadCap:xref href="../8_API Knowledge Primers/Request Headers.htm#Common">“Common Request Headers” on page 1</MadCap:xref>). These request headers are used either to specify a predefined—or <i>canned</i>—ACL, or to explicitly specify grantee permissions.</p>
    <h4>Specifying a Canned ACL</h4>
    <p style="text-align: left;">
      <MadCap:variable name="General.ProductName" /> supports a set of canned ACLs, each of which has a predefined set of grantees and permissions.</p>
    <p style="text-align: left;">To grant access permissions by specifying canned ACLs, use the following header and specify the canned ACL name as its value. </p>
    <p class="BoxNote">If the x-amz-acl header is in use, other access control specific headers in the request are ignored.</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 69px;"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-acl</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>The canned ACL to apply to the bucket you are creating</p>
            <p>Default: <code>private</code></p>
            <p>Valid Values: <code>private</code> | <code>public-read</code> | <code>public-read-write</code>  | <code>authenticated-read</code> | <code>bucket-owner-read</code> | <code>bucket-owner-full-control</code></p>
            <p>Constrains: None</p>
          </td>
        </tr>
      </tbody>
    </table>
    <h4>Explicitly Specifying Grantee Access Permissions</h4>
    <p style="text-align: left;">A set of <span class="ElementName">x-amz-grant-permission</span> headers is available for explicitly granting individualized bucket access permissions to specific <MadCap:variable name="General.ProductName" /> accounts or groups. Each of these headers maps to specific permissions the <MadCap:variable name="General.ProductName" /> supports in an ACL.</p>
    <p class="BoxNote">  If an x-amz-acl header is sent all ACL-specific headers are ignored in favor of the canned ACL.</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-read</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to list the objects in the bucket </p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-write</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to create, overwrite, and delete any object in the bucket</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-read-acp</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to read the bucket ACL</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-write-acp</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to write the ACL for the applicable bucket</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-grant-full-control</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Allows grantee the READ, WRITE, READ_ACP, and WRITE_ACP permissions on the ACL</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
      </tbody>
    </table>
    <p style="text-align: left;">For each header, the value is a comma-separated list of one or more grantees. Each grantee is specified as a <code>type=value</code> pair, where the type can be one any one of the following:</p>
    <ul>
      <li>
        <code>emailAddress</code> (if value specified is the email address of an account)</li>
      <li>
        <code>id</code> (if value specified is the canonical user ID of an account)</li>
      <li>
        <code>uri</code> (if granting permission to a predefined Amazon S3 group)</li>
    </ul>
    <p style="text-align: left;page-break-after: avoid;">For example, the following x-amz-grant-write header grants create, overwrite, and delete objects permission to a <span class="ElementName">LogDelivery</span> group predefined by <MadCap:variable name="General.ProductName" /> and two accounts identified by their email addresses.</p>
    <pre>x-amz-grant-write: uri="http://acs.amazonaws.com/groups/s3/LogDelivery", emailAddress="xyz@scality.com", emailAddress="abc@scality.com"</pre>
    <p class="BoxNote" style="text-align: left;">Though cited here for purposes of example, the <span class="ElementName">LogDelivery</span> group permission is not currently being used by <MadCap:variable name="General.ProductName" />.</p>
    <h3 MadCap:autonum="1.1.4  ">Request Elements</h3>
    <p style="text-align: left;">If the request body is used to specify an ACL, the following elements must be used.</p>
    <p class="BoxNote" style="text-align: left;">If the request body is requested, the request headers cannot be used to set an ACL.</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 120px;" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 80px;" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Element</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>AccessControlList</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>container</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Container for Grant, Grantee, and Permission</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>AccessControlPolicy</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Contains the elements that set the ACL permissions for an object per grantee
</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>DisplayName</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Screen name of the bucket owner</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>Grant</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>container</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Container for the grantee and his or her permissions</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>Grantee</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>The subject whose permissions are being set</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>ID</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>ID of the bucket owner, or the ID of the grantee</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>Owner</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>container</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Container for the bucket owner’s display name and ID</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>Permission</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Specifies the permission given to the grantee.</p>
          </td>
        </tr>
      </tbody>
    </table>
    <h4>Grantee Values</h4>
    <p style="text-align: left;">Specify the person (grantee) to whom access rights are being assigned (using request elements):</p>
    <ul>
      <li style="text-align: left;">
        <p>
          <b>By ID</b>
        </p>
        <pre>&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;&lt;ID&gt;{{ID}}&lt;/ID&gt;&lt;DisplayName&gt;GranteesEmail&lt;/DisplayName&gt;&lt;/Grantee&gt;</pre>
        <p style="text-align: left;">
          <span class="ElementName">DisplayName</span> is optional and is ignored in the request.</p>
      </li>
      <li>
        <p>
          <b>By Email Address</b>
        </p>
        <pre>&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ScalityCustomerByEmail"&gt;&lt;EmailAddress&gt;{{Grantees@email.com}}&lt;/EmailAddress&gt;lt;/Grantee&gt;</pre>
        <p style="text-align: left;">The grantee is resolved to the <span class="ElementName">CanonicalUser</span> and, in a response to a <span class="ElementName">GET Object acl</span> request, appears as the <span class="ElementName">CanonicalUser</span>.</p>
      </li>
      <li>
        <p>
          <b>By URI</b>
        </p>
        <pre>&lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;&lt;URI&gt;{{http://acs.s3.scality.com/groups/global/AuthenticatedUsers}}&lt;/URI&gt;&lt;/Grantee&gt;</pre>
      </li>
    </ul>
    <h2 MadCap:autonum="1.2.  ">Responses</h2>
    <h3 MadCap:autonum="1.2.1  ">Response Headers</h3>
    <p style="text-align: left;">Implementation of the PUT Bucket ACL operation uses only response headers that are common to all operations (refer to <MadCap:xref href="../8_API Knowledge Primers/Response Headers.htm">“Response Headers” on page 1</MadCap:xref>).</p>
    <h3 MadCap:autonum="1.2.2  ">Response Elements</h3>
    <p style="text-align: left;">The PUT Bucket ACL operation does not return response elements.</p>
    <h2 MadCap:autonum="1.3.  ">Examples</h2>
    <h3 MadCap:autonum="1.3.1  ">Access Permissions Specified in the Body</h3>
    <p style="text-align: left;">The request sample grants access permission to the existing <span class="ElementName">example-bucket</span> bucket, specifying the ACL in the body. In addition to granting full control to the bucket owner, the XML specifies the following grants.</p>
    <ul>
      <li>Grant <span class="ElementName">AllUsers</span> group READ permission on the bucket.</li>
      <li>Grant the <span class="ElementName">LogDelivery</span> group WRITE permission on the bucket.</li>
      <li>Grant an AWS account, identified by email address, WRITE_ACP permission.</li>
      <li>Grant an AWS account, identified by canonical user ID, READ_ACP permission.</li>
    </ul>
    <h4>Request Sample</h4>
    <pre xml:space="preserve">PUT ?acl HTTP/1.1
Host: example-bucket.s3.scality.com
Content-Length: 1660
x-amz-date: Thu, 12 Apr 2012 20:04:21 GMT
Authorization: {{authorizationString}}

&lt;AccessControlPolicy xmlns="http://s3.scality.com/doc/2006-03-01/"&gt;
  &lt;Owner&gt;
    &lt;ID&gt;852b113e7a2f25102679df27bb0ae12b3f85be6BucketOwnerCanonicalUserID&lt;/ID&gt;
    &lt;DisplayName&gt;OwnerDisplayName&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;852b113e7a2f25102679df27bb0ae12b3f85be6BucketOwnerCanonicalUserID&lt;/ID&gt;
        &lt;DisplayName&gt;OwnerDisplayName&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;FULL_CONTROL&lt;/Permission&gt;
    &lt;/Grant&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;
        &lt;URI xmlns=""&gt;http://acs.scality.com/groups/global/AllUsers&lt;/URI&gt;
      &lt;/Grantee&gt;
      &lt;Permission xmlns=""&gt;READ&lt;/Permission&gt;
    &lt;/Grant&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;
        &lt;URI xmlns=""&gt;http://acs.scality.com/groups/s3/LogDelivery&lt;/URI&gt;
      &lt;/Grantee&gt;
      &lt;Permission xmlns=""&gt;WRITE&lt;/Permission&gt;
    &lt;/Grant&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="AmazonCustomerByEmail"&gt;
        &lt;EmailAddress xmlns=""&gt;xyz@amazon.com&lt;/EmailAddress&gt;
      &lt;/Grantee&gt;
      &lt;Permission xmlns=""&gt;WRITE_ACP&lt;/Permission&gt;
    &lt;/Grant&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID xmlns=""&gt;f30716ab7115dcb44a5ef76e9d74b8e20567f63TestAccountCanonicalUserID&lt;/ID&gt;
      &lt;/Grantee&gt;
      &lt;Permission xmlns=""&gt;READ_ACP&lt;/Permission&gt;
    &lt;/Grant&gt;
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt;</pre>
    <h4>Response Sample</h4>
    <pre xml:space="preserve" style="page-break-inside: avoid;">HTTP/1.1 200 OK
x-amz-id-2: NxqO3PNiMHXXGwjgv15LLgUoAmPVmG0xtZw2sxePXLhpIvcyouXDrcQUaWWXcOK0
x-amz-request-id: C651BC9B4E1BD401
Date: Thu, 12 Apr 2012 20:04:28 GMT
Content-Length: 0
Server: ScalityS3</pre>
    <h3 MadCap:autonum="1.3.2  ">Access Permissions Specified Using Headers</h3>
    <p style="text-align: left;">The request sample uses ACL-specific request headers to grant the following permissions:</p>
    <ul>
      <li>Write permission to the <MadCap:variable name="General.ProductName" /><span class="ElementName">LogDelivery</span> group and an account identified by the email xyz@scality.com</li>
      <li>Read permission to the <MadCap:variable name="General.ProductName" /><span class="ElementName">AllUsers</span> group</li>
    </ul>
    <h4>Request Sample</h4>
    <pre xml:space="preserve" style="page-break-inside: avoid;">PUT ?acl HTTP/1.1
Host: example-bucket.s3.scality.com
x-amz-date: Sun, 29 Apr 2012 22:00:57 GMT
x-amz-grant-write: uri="http://acs.s3.scality.com/groups/s3/LogDelivery", emailAddress="xyz@scality.com"
x-amz-grant-read: uri="http://acs.s3.scality.com/groups/global/AllUsers"
Accept: */*
Authorization: {{authorizationString}}</pre>
    <h4>Response Sample</h4>
    <pre xml:space="preserve" style="page-break-inside: avoid;">HTTP/1.1 200 OK
x-amz-id-2: 0w9iImt23VF9s6QofOTDzelF7mrryz7d04Mw23FQCi4O205Zw28Zn+d340/RytoQ
x-amz-request-id: A6A8F01A38EC7138
Date: Sun, 29 Apr 2012 22:01:10 GMT
Content-Length: 0
Server: ScalityS3</pre>
  </body>
</html>