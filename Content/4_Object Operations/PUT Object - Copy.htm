<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
  <head>
    <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
  </head>
  <body>
    <h1 MadCap:autonum="1.  ">PUT Object - Copy</h1>
    <p>An implementation of the PUT operation, PUT Object - Copy creates a copy of an object that is already stored. On internal data backends, performing a PUT copy operation is the same as performing GET and then PUT. On external cloud data backends, data is directly copied to the designated backend. Adding the <code>x-amz-copysource</code> request header causes the PUT operation to copy the source object into the destination bucket.</p>
    <p>By default, <code>x-amz-copy-source</code> identifies the current version of an object to copy. To copy a different version, use the <code>versionId </code>subresource.</p>
    <p>When copying an object, it is possible to preserve most of the metadata (default behavior) or specify new metadata with the <code>x-amz-metadata-directive</code> header. In the case of copying an object to a specific location constraint, the metadata directive must be set to REPLACE and the location constraint header specified. Otherwise, the default location for the object copied is the location constraint of the destination bucket.</p>
    <p>The ACL, however, is not preserved and is set to private for the user making the request if no other ACL preference is sent with the request.</p>
    <p style="text-align: left;">All copy requests must be authenticated and cannot contain a message body. Additionally, READ access is required for the source object, and WRITE access is required for the destination bucket.</p>
    <p style="text-align: left;">To copy an object only under certain conditions, such as whether the ETag matches or whether the object was modified before or after a specified date, use the request headers <span class="ElementName">x-amz-copy-source-if-match</span>,<span class="ElementName"> x-amz-copy-source-if-none-match</span>,<span class="ElementName"> x-amz-copy-source-if-unmodified-since</span>, or <span class="ElementName">x-amz-copy-source-if-modified-since</span>.</p>
    <p class="BoxIMPORTANT">When using v4 Authentication all headers prefixed with x-amz- must be signed, including <span class="ElementName">x-amz-copy-source</span>.</p>
    <p style="text-align: left;">The source object being copied can be encrypted or unencrypted and the destination object can be stored encrypted or unencrypted. If bucket encryption is activated on the source bucket, the source object will remain encrypted in its original location.  If bucket encryption is activated on the destination bucket, the destination object will be encrypted.  If bucket encryption is not activated on the destination bucket, the object copy will be stored unencrypted</p>
    <p style="text-align: left;">If the copy is successful, a response will generate that contains information about the copied object.</p>
    <h2 MadCap:autonum="1.1.  ">Access Permissions</h2>
    <p style="text-align: left;">When copying an object, it is possible to specify the accounts or groups that should be granted specific permissions on the new object. There are two ways to grant the permissions using the request headers:</p>
    <ul>
      <li>Specify a canned ACL using the <span class="ElementName">x-amz-acl</span> request header.</li>
      <li>Specify access permissions explicitly using the<span class="ElementName"> x-amz-grant-read</span>, <span class="ElementName">x-amz-grant-read-acp</span>, <span class="ElementName">x-amz-grant-write-acp</span>, and <span class="ElementName">x-amz-grant-full-control</span> headers. These headers map to the set of permissions <MadCap:variable name="General.ProductName" /> supports in an ACL.</li>
    </ul>
    <p class="BoxNote">Access permissions can be explicitly specified or they can be enacted via a canned ACL. Both methods, however, cannot be deployed at the same time.</p>
    <h2 MadCap:autonum="1.2.  ">Requests</h2>
    <h3 MadCap:autonum="1.2.1  ">Request Syntax</h3>
    <p style="text-align: left;">The request syntax that follows is for sending the ACL in the request body. If headers are used to specify the permissions for the object,  the ACL cannot be sent in the request body (refer to <MadCap:xref href="../8_API Knowledge Primers/Request Headers.htm">“Request Headers” on page 1</MadCap:xref> for a list of available headers).</p>
    <pre xml:space="preserve">PUT /destinationObject HTTP/1.1
Host: destinationBucket.s3.amazonaws.com
x-amz-copy-source: /source_bucket/sourceObject
x-amz-metadata-directive: metadata_directive
x-amz-copy-source-if-match: etag
x-amz-copy-source-if-none-match: etag
x-amz-copy-source-if-unmodified-since: time_stamp
x-amz-copy-source-if-modified-since: time_stamp
&lt;request metadata&gt;
Authorization: {{authorizationString}}
Date: date</pre>
    <p class="BoxNote">The syntax shows only a representative sample of the possible request headers. For a complete list, see <MadCap:xref href="../8_API Knowledge Primers/Request Headers.htm">“Request Headers” on page 1</MadCap:xref>.</p>
    <h3 MadCap:autonum="1.2.2  ">Request Parameters</h3>
    <p style="text-align: left;">The PUT Object - Copy operation does not use request parameters.</p>
    <h3 MadCap:autonum="1.2.3  ">
      <a name="Request"></a>Request Headers</h3>
    <p style="text-align: left;"> The PUT Object - Copy operation can use a number of optional request headers in addition to those that are common to all operations (refer to <MadCap:xref href="../8_API Knowledge Primers/Request Headers.htm#Common">“Common Request Headers” on page 1</MadCap:xref>). </p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 162px;"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-copy-source</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>The name of the source bucket and key name of the source object, separated by a slash (/). If versioning is enabled, this will copy the latest version of the key by default. To specify another version append ?versionId={{version id}} after the object key.</p>
            <p>Default: None</p>
            <p>Constraints: This string must be URL-encoded. Additionally, the source bucket must be valid and READ access to the valid source object is required.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-metadata-directive</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Specifies whether the metadata is copied from the source object or replaced with metadata provided in the request.</p>
            <p>If copied, the metadata, except for the version ID, remains unchanged. In addition, the server-side-encryption, storage-class, and website-redirect-location metadata from the source is not copied. If you specify this metadata explicitly in the copy request, <MadCap:variable name="General.ProductName" /> adds this metadata to the resulting object. If you specify headers in the request specifying any user-defined metadata, the connector ignores these headers. To use new user-defined metadata, REPLACE must be selected.</p>
            <p>If replaced, all original metadata is replaced by the specified metadata.</p>
            <p>Default: COPY</p>
            <p>Valid values: COPY, REPLACE</p>
            <p>Constraints: Values other than COPY or REPLACE result in an immediate 400-based error response. An object cannot be copied to itself unless the MetadataDirective header is specified and its value set to REPLACE (or, at the least, some metadata is changed, such as storage class).</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-copy-source-if-match</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Copies the object if its entity tag (ETag) matches the specified tag; otherwise, the request returns a 412 HTTP status code error (failed precondition).</p>
            <p>Default: None</p>
            <p>Constraints: Can be used with<span class="ElementName"> x-amz-copy-source-if-unmodified-since</span>, but cannot be used with other conditional copy headers.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-copy-source-if-none-match</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Copies the object if its entity tag (ETag) is different than the specified ETag; otherwise, the request returns a 412 HTTP status code error (failed precondition).</p>
            <p>Default: None</p>
            <p>Constraints: Can be used with <span class="ElementName">x-amz-copy-source-if-modified-since</span>, but cannot be used with other conditional copy headers.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-copy-source-if-unmodified-since</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Copies the object if it hasn't been modified since the specified time; otherwise, the request returns a 412 HTTP status code error (failed precondition).</p>
            <p>Default: None</p>
            <p>Constraints: This must be a valid HTTP date. This header can be used with <span class="ElementName">x-amz-copy-source-if-match</span>, but cannot be used with other conditional copy headers.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-copy-source-if-modified-since</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Copies the object if it has been modified since the specified time; otherwise, the request returns a 412 HTTP status code error (failed condition).</p>
            <p>Default: None</p>
            <p>Constraints: This must be a valid HTTP date. This header can be used with <span class="ElementName">x-amz-copy-source-if-none-match</span>, but cannot be used with other conditional copy headers.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-storage-class</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>enum</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Standard is the default storage class, unless specified otherwise. Currently, Scality <MadCap:variable name="General.ProductName" /> only suports one level of storage class.</p>
            <p>Default: Standard</p>
            <p>Valid Values: STANDARD, STANDARD_IA, REDUCED_REDUNDANCY</p>
          </td>
        </tr>
      </tbody>
    </table>
    <p style="text-align: left;">Note the following additional considerations about the preceding request headers:</p>
    <ul>
      <li>
        <p>
          <span class="Strong">Consideration 1:</span> If both of the<span class="ElementName"> x-amz-copy-source-if-match</span> and <span class="ElementName">x-amz-copy-source-if-unmodified-since</span> headers are present in the request as follows, <MadCap:variable name="General.ProductName" /> returns 200 OK and copies the data:</p>
        <pre xml:space="preserve">x-amz-copy-source-if-match condition evaluates to true, and;
x-amz-copy-source-if-unmodified-since condition evaluates to false;</pre>
      </li>
    </ul>
    <ul>
      <li>
        <p>
          <span class="Strong">Consideration 2:</span> If both of the <span class="ElementName">x-amz-copy-source-if-none-match</span> and <span class="ElementName">x-amz-copy-source-if-modified-since</span> headers are present in the request as follows, <MadCap:variable name="General.ProductName" /> returns a 412 Precondition Failed response code:</p>
        <pre xml:space="preserve">x-amz-copy-source-if-none-match condition evaluates to false, and;
x-amz-copy-source-if-modified-since condition evaluates to true</pre>
      </li>
    </ul>
    <p style="text-align: left;"> Additionally, the following access control-related (ACL) headers can be used with the PUT Object - Copy operation. By default, all objects are private; only the owner has full access control. When adding a new object, it is possible to grant permissions to individual AWS accounts or predefined groups defined by Amazon S3. These permissions are then added to the Access Control List (ACL) on the object. For more information, refer to <MadCap:xref href="../8_API Knowledge Primers/ACL (Access Control List) Primer.htm">“ACL (Access Control List)” on page 1</MadCap:xref>. </p>
    <h4>Specifying a Canned ACL</h4>
    <p style="text-align: left;">
      <MadCap:variable name="General.ProductName" /> supports a set of predefined ACLs, each of which has a predefined set of grantees and permissions.</p>
    <p style="text-align: left;">To grant access permissions by specifying canned ACLs, use the <span class="ElementName">x-amz-acl</span> header and specify the canned ACL name as its value. </p>
    <p class="BoxNote"> Other access control specific headers cannot be used when the <span class="ElementName">x-amz-acl</span> header is in use.</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 87px;"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-acl</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>The canned ACL to apply to the object.</p>
            <p>Default: <code>private</code></p>
            <p>Valid Values: <code>private</code> | <code>public-read</code> | <code>public-read-write</code> | <code>aws-exec-read</code> | <code>authenticated-read</code> | <code>bucket-owner-read</code> | <code>bucket-owner-full-control</code></p>
            <p>Constrains: None</p>
          </td>
        </tr>
      </tbody>
    </table>
    <h4>Explicitly Specifying Grantee Access Permissions</h4>
    <p style="text-align: left;">A set of headers is available for explicitly granting access permissions to specific <MadCap:variable name="General.ProductName" /> accounts or groups.</p>
    <p class="BoxNote">Each of the <span class="ElementName">x-amz-grant-permission</span> headers maps to specific permissions the <MadCap:variable name="General.ProductName"></MadCap:variable> supports in an ACL.  Please also note that the use of any of these ACL-specific headers negates the use of the <span class="ElementName">x-amz-acl</span> header to set a canned ACL.</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <col class="TableStyle-DetailedwithPadding-Column-Column1"></col>
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-read</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to read the object data and its metadata</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-write</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Not applicable. This applies only when granting access permissions on a bucket.</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-read-acp</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to read the object ACL</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>x-amz-grant-write-acp</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Allows grantee to write the ACL for the applicable object</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-grant-full-control</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Allows grantee the READ, READ_ACP, and WRITE_ACP permissions on the object</p>
            <p>Default: None </p>
            <p>Constraints: None</p>
          </td>
        </tr>
      </tbody>
    </table>
    <p style="text-align: left;">For each header, the value is a comma-separated list of one or more grantees. Each grantee is specified as a <code>type=value</code> pair, where the type can be one any one of the following:</p>
    <ul>
      <li>
        <code>emailAddress</code> (if value specified is the email address of an account)</li>
      <li>
        <code>id</code> (if value specified is the canonical user ID of an account)</li>
      <li>
        <code>uri</code> (if granting permission to a predefined group)</li>
    </ul>
    <p style="text-align: left;">For example, the following <span class="ElementName">x-amz-grant-read</span> header grants list objects permission to two accounts identified by their email addresses:</p>
    <pre xml:space="preserve">x-amz-grant-read:  emailAddress="xyz@scality.com", emailAddress="abc@scality.com"</pre>
    <h3 MadCap:autonum="1.2.4  ">Request Elements</h3>
    <p style="text-align: left;">The implementation of the operation does not use request parameters.</p>
    <h2 MadCap:autonum="1.3.  ">Responses</h2>
    <h3 MadCap:autonum="1.3.1  ">Response Headers</h3>
    <p class="pgBreakKeepNext" style="text-align: left;">Implementation of the PUT Object - Copy operation can include the following response headers in addition to the response headers common to all responses (refer to <MadCap:xref href="../8_API Knowledge Primers/Response Headers.htm">“Response Headers” on page 1</MadCap:xref>).</p>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 180px;" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" MadCap:conditions="PrintGuides.GA">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">x-amz-copy-source-version-id</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Returns the version ID of the retrieved object if it has a unique version ID.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">x-amz-server-side-encryption</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">If server-side encryption is specified either with an AWS KMS or <MadCap:variable name="General.ProductName" />-managed encryption key in the copy request, the response includes this header, confirming the encryption algorithm that was used to encrypt the object.</td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">x-amz-server-side-encryption-aws-kms-key-id</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>If the <span class="ElementName">x-amz-server-side-encryption</span> is present and has the value of <span class="ElementName">aws:kms</span>, this header specifies the ID of the AWS Key Management Service (KMS) master encryption key that was used for the object.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">x-amz-server-side-encryption-customer-algorithm</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>If server-side encryption with customer-provided encryption keys (SSE-C) encryption was requested, the response will include this header confirming the encryption algorithm used for the destination object.</p>
            <p>Valid Values: <code>AES256</code></p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">x-amz-server-side-encryption-customer-key-MD5</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>If SSE-C encryption was requested, the response includes this header to provide roundtrip message integrity verification of the customer-provided encryption key used to encrypt the destination object.</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>x-amz-version-id</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
            <p>string</p>
          </td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Version of the copied object in the destination bucket.</p>
          </td>
        </tr>
      </tbody>
    </table>
    <h3 MadCap:autonum="1.3.2  ">Response Elements</h3>
    <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <col class="TableStyle-DetailedwithPadding-Column-Column1" />
      <thead>
        <tr class="TableStyle-DetailedwithPadding-Head-Header1">
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Header</th>
          <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1" MadCap:conditions="PrintGuides.GA">Type</th>
          <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1" MadCap:conditions="PrintGuides.GA">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">CopyObjectResult</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">container</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Container for all response elements.</p>
            <p>Ancestor: None</p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">ETag</td>
          <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
            <p>Returns the <span class="ElementName">ETag</span> of the new object. The <span class="ElementName">ETag</span> reflects changes only to the contents of an object, not its metadata. The source and destination <span class="ElementName">ETag</span> will be identical for a successfully copied object.</p>
            <p>Ancestor: <code>CopyObjectResult</code></p>
          </td>
        </tr>
        <tr class="TableStyle-DetailedwithPadding-Body-Body1">
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">LastModified</td>
          <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">string</td>
          <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
            <p>Returns the date the object was last modified.</p>
            <p>Ancestor: <code>CopyObjectResult</code></p>
          </td>
        </tr>
      </tbody>
    </table>
    <h2 MadCap:autonum="1.4.  ">Examples</h2>
    <h3 MadCap:autonum="1.4.1  ">Copying a File into a Bucket with a Different Key Name</h3>
    <p style="text-align: left;">The request sample copies a pdf file into a bucket with a different key name. </p>
    <h4>Request Sample</h4>
    <pre xml:space="preserve">PUT /my-document.pdf HTTP/1.1
Host: {{bucketName}}.s3.scality.com
Date: Wed, 21 Sep 2016 18:18:00 GMT
x-amz-copy-source: /{{bucketName}}/my-pdf-document.pdf
Authorization: {{authorizationString}}</pre>
    <h4>Response Sample</h4>
    <pre xml:space="preserve">HTTP/1.1 200 OK
x-amz-id-2: eftixk72aD6Ap51TnqcoF8eFidJG9Z/2mkiDFu8yU9AS1ed4OpIszj7UDNEHGran
x-amz-request-id: 318BC8BC148832E5
x-amz-copy-source-version-id: 3/L4kqtJlcpXroDTDmJ+rmSpXd3dIbrHY+MTRCxf3vjVBH40Nr8X8gdRQBpUMLUo
x-amz-version-id: QUpfdndhfd8438MNFDN93jdnJFkdmqnh893
Date: Wed, 21 Sep 2016 18:18:00 GMT
Connection: close
Server: ScalityS3</pre>
    <pre xml:space="preserve">
&lt;CopyObjectResult&gt;
   &lt;LastModified&gt;2009-10-28T22:32:00&lt;/LastModified&gt;
   &lt;ETag&gt;"9b2cf535f27731c974343645a3985328"&lt;/ETag&gt;
&lt;/CopyObjectResult&gt;</pre>
    <p style="text-align: left;">
      <span class="ElementName">x-amz-version-id</span> returns the version ID of the object in the destination bucket, and <span class="ElementName">x-amz-copy-source-version-id</span> returns the version ID of the source object.</p>
    <h3 MadCap:autonum="1.4.2  ">Copying a Specified Version of an Object</h3>
    <p style="text-align: left;">The request sample copies a pdf file with a specified version ID and copies it into the bucket {{bucketname}} and gives it a different key name.</p>
    <h4>Request Sample</h4>
    <pre>PUT /my-document.pdf HTTP/1.1
Host: {{bucketName}}.s3.scality.com
Date: Wed, 21 Sep 2016 18:18:00 GMT
x-amz-copy-source: /{{bucketName}}/my-pdf-document.pdf?versionId=3/L4kqtJlcpXroDTDmJ+rmSpXd3dIbrHY+MTRCxf3vjVBH40Nr8X8gdRQBpUMLUo
Authorization: {{authorizationString}}</pre>
    <h4>Response Sample: Copying a Versioned Object into a Version-Enabled Bucket</h4>
    <p style="text-align: left;">The response sample shows that an object was copied into a target bucket where Versioning is enabled.</p>
    <pre xml:space="preserve">HTTP/1.1 200 OK
x-amz-id-2: eftixk72aD6Ap51TnqcoF8eFidJG9Z/2mkiDFu8yU9AS1ed4OpIszj7UDNEHGran
x-amz-request-id: 318BC8BC148832E5
x-amz-version-id: QUpfdndhfd8438MNFDN93jdnJFkdmqnh893
x-amz-copy-source-version-id: 09df8234529fjs0dfi0w52935029wefdj
Date: Wed, 21 Sep 2016 18:18:00 GMT
Connection: close
Server: ScalityS3</pre>
    <pre xml:space="preserve">

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;CopyObjectResult&gt;
   &lt;LastModified&gt;2009-10-28T22:32:00&lt;/LastModified&gt;
   &lt;ETag&gt;"9b2cf535f27731c974343645a3985328"&lt;/ETag&gt;
&lt;/CopyObjectResult&gt;</pre>
    <h4>Response Sample: Copying a Versioned Object into a Version-Suspended Bucket</h4>
    <p style="text-align: left;">The response sample shows that an object was copied into a target bucket where versioning is suspended. Note that the response header <span class="ElementName">x-amz-version-id</span> does not appear.</p>
    <pre xml:space="preserve">
HTTP/1.1 200 OK
x-amz-id-2: eftixk72aD6Ap51TnqcoF8eFidJG9Z/2mkiDFu8yU9AS1ed4OpIszj7UDNEHGran
x-amz-request-id: 318BC8BC148832E5
x-amz-copy-source-version-id: 3/L4kqtJlcpXroDTDmJ+rmSpXd3dIbrHY+MTRCxf3vjVBH40Nr8X8gdRQBpUMLUo
Date: Wed, 21 Sep 2016 18:18:00 GMT
Connection: close
Server: ScalityS3</pre>
    <pre xml:space="preserve">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;CopyObjectResult&gt;
  &lt;LastModified&gt;2009-10-28T22:32:00&lt;/LastModified&gt;
  &lt;ETag&gt;"9b2cf535f27731c974343645a3985328"&lt;/ETag&gt;
&lt;/CopyObjectResult&gt;</pre>
    <h3 MadCap:autonum="1.4.3  ">Copying from an Unencrypted Object to an Object Encrypted with Server-Side Encryption, Using Customer-Provided Encryption Keys</h3>
    <p style="text-align: left;">The request sample specifies the HTTP PUT header to copy an unencrypted object to an object encrypted with server-side encryption with customer-provided encryption keys (SSE-C).</p>
    <h4>Request Sample</h4>
    <pre xml:space="preserve">PUT ExampleObject.txt?acl HTTP/1.1
Host: {{bucketName}}.s3.scality.com
x-amz-acl: public-read
Accept: */*
Authorization: {{authorizationString}}
Host: s3.scality.com
Connection: Keep-Alive
PUT /exampleDestinationObject HTTP/1.1
Host: example-destination-bucket.s3.amazonaws.com
x-amz-server-side-encryption-customer-algorithm: AES256
x-amz-server-side-encryption-customer-key: Base64{{customerProvidedKey}})
x-amz-server-side-encryption-customer-key-MD5 : Base64(MD5{{customerProvidedKey}})
x-amz-metadata-directive: metadata_directive
x-amz-copy-source: /example_source_bucket/exampleSourceObject
x-amz-copy-source-if-match: {{etag}}
x-amz-copy-source-if-none-match: {{etag}}
x-amz-copy-source-if-unmodified-since: {{timeStamp}}
x-amz-copy-source-if-modified-since: {{timeStamp}}
&lt;request metadata&gt;
Authorization: {{authorizationString}}
Date: {{date}}</pre>
    <h3 MadCap:autonum="1.4.4  ">Copying from an Object Encrypted with SSE-C to an Object Encrypted with SSE-C</h3>
    <p style="text-align: left;">The request sample specifies the HTTP PUT header to copy an object encrypted with server-side encryption with customer-provided encryption keys to an object encrypted with server-side encryption with customer-provided encryption keys for key rotation.</p>
    <h4>Request Sample</h4>
    <pre xml:space="preserve">PUT /exampleDestinationObject HTTP/1.1
Host: example-destination-bucket.s3.amazonaws.com
x-amz-server-side-encryption-customer-algorithm: AES256
x-amz-server-side-encryption-customer-key: Base64({{customerProvidedKey}})
x-amz-server-side-encryption-customer-key-MD5: Base64(MD5{{customerProvidedKey}})
x-amz-metadata-directive: metadata_directive
x-amz-copy-source: /source_bucket/sourceObject
x-amz-copy-source-if-match: {{etag}}
x-amz-copy-source-if-none-match: {{etag}}
x-amz-copy-source-if-unmodified-since: {{timeStamp}}
x-amz-copy-source-if-modified-since: {{timeStamp}}
x-amz-copy-source-server-side-encryption-customer-algorithm: AES256
x-amz-copy-source-server-side-encryption-customer-key: Base64({{oldKey}})
x-amz-copy-source-server-side-encryption-customer-key-MD5: Base64(MD5{{oldKey}})
&lt;request metadata&gt;
Authorization: {{authorizationString}}
Date: {{date}}</pre>
  </body>
</html>